import java.util.Arrays;

public class Main {

    private static void stepFill(char arr[], char l) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = l;
            i++;
        }
    }

    public static void main(String[] args) {

        int col, row = 1000;
        col = row;
        char black = 'ч';
        char white = 'б';

        char[][] arr = new char[col][row];
        char[] arr_temp = new char[row + 1];

        Arrays.fill(arr_temp, white);

        stepFill(arr_temp, black);

        for (int i = 0; i < col; i++) {
            if (i % 2 == 0) {
                System.arraycopy(arr_temp, 1, arr[i], 0, row);
            } else {
                System.arraycopy(arr_temp, 0, arr[i], 0, row);
            }
            System.out.println(arr[i]);
        }
    }
}
